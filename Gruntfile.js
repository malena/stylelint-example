module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		watch: {
			options:{
				livereload: true,
			},
			less: {
				files: ['less/*.less'],
				tasks: ['less', 'postcss'],
				options: {
					nospawn: true
				}
			}
		},
		less: {
			compile: {
				files: {
					'less/index.css' : 'less/index.less'
				}
			}
		},
		postcss: {
		    options: {
				processors: [
					require('stylelint')(),
					require('postcss-reporter')({
                        clearMessages: true
                    })
				]
			},
		    dist: {
		    	src: 'less/index.css',
		    	dest: 'public/project.css'
		    }
		},
		connect: {
            server: {
                options: {
                    hostname: 'localhost',
                    port: 8800
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['connect', 'less', 'postcss', 'watch']);
};
